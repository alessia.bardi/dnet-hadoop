{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "definitions": {
    "ControlledField": {
      "type": "object",
      "properties": {
        "scheme": {
          "type": "string"
        },
        "value": {
          "type": "string"
        }
      },
      "description": "To represent the information described by a scheme and a value in that scheme (i.e. pid)"
    }
  },
  "type": "object",
  "properties": {
    "accessrights": {
      "type": "string",
      "description": "Type of access to the data source, as defined by re3data.org. Possible values: {open, restricted, closed}"
    },
    "certificates": {
      "type": "string",
      "description": "The certificate, seal or standard the data source complies with. As defined by re3data.org."
    },
    "citationguidelineurl": {
      "type": "string",
      "description":"The URL of the data source providing information on how to cite its items. As defined by re3data.org."
    },
    "contenttypes": {
      "description": "Types of content in the data source, as defined by OpenDOAR",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "databaseaccessrestriction": {
      "type": "string",
      "description": "Access restrinctions to the data source, as defined by re3data.org. One of {feeRequired, registration, other}"
    },
    "datasourcetype": {
      "allOf": [
        {
          "$ref": "#/definitions/ControlledField"
        },
        {
          "description": "The type of the datasource. See https://api.openaire.eu/vocabularies/dnet:datasource_typologies"
        }
      ]
    },
    "datauploadrestriction": {
      "type": "string",
      "description": "Upload restrictions applied by the datasource, as defined by re3data.org. One of {feeRequired, registration, other}"
    },
    "dateofvalidation": {
      "type": "string",
      "description": "The date of last validation against the OpenAIRE guidelines for the datasource records"
    },
    "description": {
      "type": "string"
    },
    "englishname": {
      "type": "string",
      "description": "The English name of the datasource"
    },
    "id": {
      "type": "string",
      "description": "The OpenAIRE id of the data source"
    },
    "journal": {
      "type": "object",
      "properties": {
        "conferencedate": {
          "type": "string"
        },
        "conferenceplace": {
          "type": "string"
        },
        "edition": {
          "type": "string"
        },
        "ep": {
          "type": "string",
          "description": "End page"
        },
        "iss": {
          "type": "string",
          "description": "Issue number"
        },
        "issnLinking": {
          "type": "string"
        },
        "issnOnline": {
          "type": "string"
        },
        "issnPrinted": {
          "type": "string"
        },
        "name": {
          "type": "string"
        },
        "sp": {
          "type": "string",
          "description": "Start page"
        },
        "vol": {
          "type": "string",
          "description": "Volume"
        }
      },
      "description": "Information about the journal, if this data source is of type Journal."
    },
    "languages": {
      "description": "The languages present in the data source's content, as defined by OpenDOAR.",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "logourl": {
      "type": "string"
    },
    "missionstatementurl": {
      "type": "string",
      "description":"The URL of a mission statement describing the designated community of the data source. As defined by re3data.org"
    },
    "officialname": {
      "type": "string",
      "description": "The official name of the datasource"
    },
    "openairecompatibility": {
      "type": "string",
      "description": "OpenAIRE guidelines the data source comply with. See also https://guidelines.openaire.eu."
    },
    "originalId": {
      "description": "Original identifiers for the datasource"
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "pid": {
      "description": "Persistent identifiers of the datasource",
      "type": "array",
      "items": {
        "allOf": [
          {
            "$ref": "#/definitions/ControlledField"
          }
        ]
      }
    },
    "pidsystems": {
      "type": "string",
      "description": "The persistent identifier system that is used by the data source. As defined by re3data.org"
    },
    "policies": {
      "description": "Policies of the data source, as defined in OpenDOAR.",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "releaseenddate": {
      "type": "string",
      "description": "Date when the data source went offline or stopped ingesting new research data. As defined by re3data.org"
    },
    "releasestartdate": {
      "type": "string",
      "description": "Releasing date of the data source, as defined by re3data.org"
    },
    "subjects": {
      "description": "List of subjects associated to the datasource",
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "uploadrights": {
      "type": "string",
      "description": "Type of data upload. As defined by re3data.org: one of {open, restricted,closed}"
    },
    "versioning": {
      "type": "boolean",
      "description": "As defined by redata.org: 'yes' if the data source supports versioning, 'no' otherwise."
    },
    "websiteurl": {
      "type": "string"
    }
  }
}
