
package eu.dnetlib.dhp.common.api.zenodo;

public class Community {
	private String identifier;

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
}
