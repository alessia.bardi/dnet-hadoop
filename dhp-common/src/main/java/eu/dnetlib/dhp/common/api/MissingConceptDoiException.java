
package eu.dnetlib.dhp.common.api;

public class MissingConceptDoiException extends Throwable {
	public MissingConceptDoiException(String message) {
		super(message);
	}
}
